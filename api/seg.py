from vmtk import vmtkscripts

# Read image
vmtkimagereader = vmtkscripts.vmtkImageReader()
vmtkimagereader.InputFileName = 'images/vti/img.vti'
vmtkimagereader.Execute()

# Level set segmentation: isosurface
vmtkimageinitialization = vmtkscripts.vmtkImageInitialization()
vmtkimageinitialization.Image = vmtkimagereader.Image
vmtkimageinitialization.Interactive = 0
vmtkimageinitialization.Method = 'isosurface'
isosurfacevalue = 510 # For tech task: the user input for isosurface value is obtained the web app!
vmtkimageinitialization.IsoSurfaceValue = float(isosurfacevalue)
vmtkimageinitialization.Execute()

vmtklevelsetsegmentation = vmtkscripts.vmtkLevelSetSegmentation()
vmtklevelsetsegmentation.Image = vmtkimagereader.Image
vmtklevelsetsegmentation.InitializationImage = vmtkimageinitialization.InitialLevelSets
vmtklevelsetsegmentation.InitialLevelSets = vmtkimageinitialization.InitialLevelSets
vmtklevelsetsegmentation.NumberOfIterations = 100
vmtklevelsetsegmentation.Execute()

vmtkimagewriter = vmtkscripts.vmtkImageWriter()
vmtkimagewriter.Image = vmtklevelsetsegmentation.LevelSets
vmtkimagewriter.OutputFileName = 'images/vti/imgLevelSet.vti'
vmtkimagewriter.Execute()

# Marching cube surface generation
vmtkmarchingcubes = vmtkscripts.vmtkMarchingCubes()
vmtkmarchingcubes.Image = vmtklevelsetsegmentation.LevelSets
vmtkmarchingcubes.Execute()

vmtksurfacewriter = vmtkscripts.vmtkSurfaceWriter()
vmtksurfacewriter.Surface = vmtkmarchingcubes.Surface
vmtksurfacewriter.OutputFileName = 'images/vtp/surf.vtp'
vmtksurfacewriter.Execute()

# Surface smoothing
vmtksurfacesmoothing = vmtkscripts.vmtkSurfaceSmoothing()
vmtksurfacesmoothing.Surface = vmtkmarchingcubes.Surface
vmtksurfacesmoothing.NumberOfIterations = 100
vmtksurfacesmoothing.PassBand = 0.1
vmtksurfacesmoothing.Execute()

vmtksurfacewriter = vmtkscripts.vmtkSurfaceWriter()
vmtksurfacewriter.Surface = vmtksurfacesmoothing.Surface
vmtksurfacewriter.OutputFileName = 'images/vtp/surf.vtp'
vmtksurfacewriter.Execute()

vmtksurfaceviewer = vmtkscripts.vmtkSurfaceViewer()
vmtksurfaceviewer.Surface = vmtksurfacewriter.Surface
vmtksurfaceviewer.Execute()
