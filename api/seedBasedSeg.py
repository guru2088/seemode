from vmtk import vmtkscripts

vmtkimagereader = vmtkscripts.vmtkImageReader()
vmtkimagereader.InputFileName = 'images/vti/imgSeedBasedSeg.vti'
vmtkimagereader.Execute()

## Seed inputs for level set segmentation: seed = [[x,y,z],intensity]
## seeds for imgSeedBasedSeg.vti
ccaSeed1 = [[212,258,102],673] # For tech task: the seed location and intensity is obtained in the web app!
ccaSeed2 = [[207,260,121],809] # For tech task: the seed location and intensity is obtained in the web app!

tolerance = 0.3 # tolerance used in calculating upper and lower thresholds for segmentation

def seedBasedSegmentation(seed1,seed2,image):

    # Level set segmentation: seed-based colliding fronts
    vmtkimageinitialization = vmtkscripts.vmtkImageInitialization()
    vmtkimageinitialization.Image = image
    vmtkimageinitialization.Interactive = 0
    vmtkimageinitialization.Method = 'collidingfronts'
    vmtkimageinitialization.SourcePoints = seed1[0]
    vmtkimageinitialization.TargetPoints = seed2[0]
    vmtkimageinitialization.UpperThreshold = max(seed1[1],seed2[1])*(1.0 + tolerance)
    vmtkimageinitialization.LowerThreshold = min(seed1[1],seed2[1])*(1.0 - tolerance)
    vmtkimageinitialization.Execute()

    vmtklevelsetsegmentation = vmtkscripts.vmtkLevelSetSegmentation()
    vmtklevelsetsegmentation.Image = image
    vmtklevelsetsegmentation.InitializationImage = vmtkimageinitialization.InitialLevelSets
    vmtklevelsetsegmentation.InitialLevelSets = vmtkimageinitialization.InitialLevelSets
    vmtklevelsetsegmentation.NumberOfIterations = 100
    vmtklevelsetsegmentation.Execute()

    return(vmtklevelsetsegmentation.LevelSets)

image = vmtkimagereader.Image
ccaLevelSets = seedBasedSegmentation(ccaSeed1,ccaSeed2,image)

# Marching cube surface generation
vmtkmarchingcubes = vmtkscripts.vmtkMarchingCubes()
vmtkmarchingcubes.Image = ccaLevelSets #vmtklevelsetsegmentation.LevelSets
vmtkmarchingcubes.Execute()

# Surface smoothing
vmtksurfacesmoothing = vmtkscripts.vmtkSurfaceSmoothing()
vmtksurfacesmoothing.Surface = vmtkmarchingcubes.Surface
vmtksurfacesmoothing.NumberOfIterations = 100
vmtksurfacesmoothing.PassBand = 0.1
vmtksurfacesmoothing.Execute()

vmtksurfacewriter = vmtkscripts.vmtkSurfaceWriter()
vmtksurfacewriter.Surface = vmtksurfacesmoothing.Surface
vmtksurfacewriter.OutputFileName = 'images/vtp/surfSeedBasedSeg.vtp'
vmtksurfacewriter.Execute()

vmtksurfaceviewer = vmtkscripts.vmtkSurfaceViewer()
vmtksurfaceviewer.Surface = vmtksurfacewriter.Surface
vmtksurfaceviewer.Execute()
