var path = require('path');
var webpack = require('webpack');
var vtkRules = require('vtk.js/Utilities/config/dependency.js').webpack.v2.rules;

// var entry = path.join(__dirname, './src/index.js');
// const sourcePath = path.join(__dirname, './src');
// const outputPath = path.join(__dirname, './dist');

module.exports = {
  entry: {
    render_vti:'./src/render_vti.js',
    visualize_vti:'./src/visualize_vti.js'
  },
  output: {
    path: path.resolve(__dirname, 'output'),
    filename: '[name].js',
  },
  module: {
    rules: [
        { test: path.join(__dirname, './src/render_vti.js'), loader: "expose-loader?MyWebApp" },
        { test: /\.html$/, loader: 'html-loader' },
        { test: /\.vue/, loader: 'vue' },
        { test: /\.mcss$/, loader: 'css-loader?modules&localIdentName=[name]__[local]&importLoaders=1'},
        { test: /\.svg$/, loader: 'svg-loader'},
        { test: /\.(jpe?g|png|gif|svg)$/i, use: ['url-loader?limit=10000','img-loader']}
    ].concat(vtkRules),
  },
  resolve: {
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.join(__dirname, './src'),
    ],
  },
};
